const {Router} = require('express');
const { body, param, check, validationResult } = require('express-validator');
// Валидатор проверяет body, cookies, headers, params, query
// check проверяет все эти типы сразу.
// https://express-validator.github.io/docs/api/validation-chain

const db = require('../db');
const User = require('../models/User');
const Cart = require('../models/Cart');
const CartUser = require('../models/CartUser');
const CartItem = require('../models/CartItem');

// Приведение типов в PostgreSQL
// https://github.com/brianc/node-postgres/issues/2674
// $1::uuid приведение к типу UUID
// $1::int (::integer) приведение к целому типу INTEGER
// $1::text приведение к типу текст TEXT, VARCHAR
// $1::date приведение к типу дата DATE
// $1::bool (::boolean) приведение к логическому типу BOOLEAN (t/f)

// CORS задаётся в server.js

const router = Router();

// Лучшие практики по проектированию API
// https://stackoverflow.blog/2020/03/02/best-practices-for-rest-api-design/

// Cart
// id - идентификатор корзины
// name - название корзины

// CartItem
// id - идентификатор элемента в корзине
// cartId - идентификатор корзины
// name - название элемента
// quantity_plan - количество элементов планируемое
// quantity_plan - количество элементов фактическое
// picked - индикатор того, что элемент собран

// req.headers  // Заголовки запроса
// req.params   // Параметры пути (:id)
// req.query    // Параметры запроса
// req.body     // Данные тела запроса
// req.cookies  // Куки

function authorize(req, res, next) {
  // console.log('req.session', req.session);
  if (req.session.user_id) {
    return next();
  }

  // res.redirect('/login');
  return res.status(403).json({
    error: {
      code: 403,
      message: 'Пользователь не авторизован',
    },
  });
}

router.get('/api/carts', /* authorize, */ async (req, res) => {
  console.log('GET /api/carts');

  if (! 'user_id' in req.session) {
    return res.status(200).json([]);
  }

  console.log('db', db); // db - BoundPool

  let select_carts_result = undefined;
  try {
    select_carts_result = await db.query({
      text: `
        SELECT
          c.id,
          c.replica_id,
          c.name
        FROM carts AS c
        LEFT JOIN carts_users AS cu ON c.id = cu.cart_id
        WHERE cu.user_id = $1::uuid
      `,
      values: [
        req.session.user_id,
      ]
    });
  }
  catch(error) {
    console.error('query carts error', error);

    return res.status(500).json({
      status: 500,
      error: `Ошибка при получении списка корзин`
    });
  }

  // console.log(select_carts_result); // [ { id: 24, name: 'test5' }, { id: 25, name: 'Корзина 1' } ]

  let carts = select_carts_result.rows;

  for (let i = 0; i < carts.length; i++) {
    let cart_id = carts[i].id;
    // console.log('cart id =', cart_id);

    let select_cart_items_result = undefined;
    try {
      select_cart_items_result = await db.query({
        text: `
          SELECT *
          FROM carts_items
          WHERE cart_id = $1
        `,
        values: [
          cart_id,
        ]
      });

      // console.log(select_cart_items_result.rows);
    }
    catch(error) {
      console.error('query cart items error', error);
  
      return res.status(500).json({
        status: 500,
        error: `Ошибка при получении списка корзин`
      });
    }

    carts[i].items = select_cart_items_result.rows;

    let select_cart_users_result = undefined;
    try {
      select_cart_users_result = await db.query({
        text: `
          SELECT
            u.id AS user_id,
            p.id AS profile_id,
            p.first_name,
            p.last_name,
            p.sex
          FROM carts_users AS cu
          LEFT JOIN users AS u ON cu.user_id = u.id
          LEFT JOIN profiles AS p ON u.profile_id = p.id
          WHERE cu.cart_id = $1
        `,
        values: [
          cart_id,
        ]
      });

      // console.log(select_cart_users_result.rows);

      let cart_users = select_cart_users_result.rows;
      let users = [];

      for (let i = 0; i < cart_users.length; i++) {
        users.push({
          id: cart_users[i].user_id,
          profile: {
            id: cart_users[i].profile_id,
            first_name: cart_users[i].first_name,
            last_name: cart_users[i].last_name,
            sex: cart_users[i].sex,
          }
        });
      }

      carts[i].users = users;
    }
    catch(error) {
      console.error('query cart items error', error);
  
      return res.status(500).json({
        status: 500,
        error: `Ошибка при получении списка корзин`
      });
    }
  }

  // console.log('carts', carts);

  return res.status(200).json(carts);
});

router.post('/api/carts', authorize, [
  body('name').trim().notEmpty()
], async (req, res) => {
  let debug = true;
  const vResult = validationResult(req);
  if (! vResult.isEmpty()) {
    return res.send({ errors: vResult.array() });
  }

  console.log('POST /api/carts');

  debug && console.log('req.body', req.body);

  debug && console.log('Создаю новый список.');
  const cart = new Cart();

  if (! 'name' in req.body) {
    return res.status(400).json({
      status: 400,
      error: "Не указано имя новой корзины"
    });
  }

  let DbClient = await db.connect(); // DbClient - Client

  debug && console.log('Беру клиента из пула. Его надо будет потом обязательно вернуть в пул.');
  cart.setDbClient(DbClient);

  debug && console.log('ТРАНЗАКЦИЯ: начало.');
  await DbClient.query('BEGIN');

  cart.setName(req.body.name);

  try {
    debug && console.log('Сохраняю список, чтобы он получил ID.');
    await cart.insert();

    debug && console.log('Связываю список с пользователем.');
    const cartUser = new CartUser({
      cart_id: cart.getId(),
      user_id: req.session.user_id,
    });

    debug && console.log('Указываю клиента БД для привязки списка к пользователю.');
    cartUser.setDbClient(DbClient);

    debug && console.log('Указываю что пользователь является владельцем.');
    cartUser.setIsOwner();

    debug && console.log('Создаю связку пользователя со списком в БД.');
    await cartUser.insert();

    if (
      'items' in req.body
      && Array.isArray(req.body.items)
    ) {
      for (let i = 0; i < req.body.items.length; i++) {
        // Коррекция ID для новых списков.
        req.body.items[i].id = 0;

        debug && console.log(`Создаю объект для элемента с индексом ${i}.`);
        let cartItem = new CartItem(req.body.items[i]);

        debug && console.log('Указываю клиента БД для привязки элемента к списку.');
        cartItem.setDbClient(DbClient);

        debug && console.log('Указываю ID списка в элементе.');
        cartItem.setCartId(cart.getId());

        debug && console.log('Создаю элемент списка в БД.');
        let insert_cart_item_result = await cartItem.insert();

        debug && console.log('insert_cart_item_result', insert_cart_item_result);

        if (insert_cart_item_result === null) {
          debug && console.log('ТРАНЗАКЦИЯ: откат.');
          await DbClient.query('ROLLBACK');
          debug && console.log('Возвращаю клиента в пул.');
          await DbClient.release();

          return res.status(400).json({
            status: 400,
            error: "Ошибка при добавлении элемента в список"
          });
        }
      }
    }
  }
  catch(error) {
    debug && console.log('ТРАНЗАКЦИЯ: откат.');
    await DbClient.query('ROLLBACK');
    debug && console.log('Возвращаю клиента в пул.');
    await DbClient.release();

    console.log('error', error);
    return res.status(400).json({
      status: 400,
      error: "Ошибка при создании корзины"
    });
  }

  debug && console.log('Получаю строку JSON для ответа.');
  let response_json_string = await cart.toExtendedObject();
  
  debug && console.log('ТРАНЗАКЦИЯ: фиксация.');
  await DbClient.query('COMMIT');
  debug && console.log('Возвращаю клиента в пул.');
  await DbClient.release();

  debug && console.log('response_json_string', response_json_string);

  return res.status(200).json(response_json_string);
});

router.get('/api/carts/:cart_id', authorize, [
  param('cart_id').trim().isDecimal()
], async (req, res) => {
  const result = validationResult(req);
  if (! result.isEmpty()) {
    return res.send({ errors: result.array() });
  }

  let cart = {};

  let cart_id = req.params.cart_id;

  console.log(`GET /api/carts/${cart_id}`);

  let select_cart_result = undefined;
  try {
    select_cart_result = await db.query({
      text: `
        SELECT
          c.id,
          c.name
        FROM carts AS c
        LEFT JOIN carts_users AS cu ON c.id = cu.cart_id
        WHERE
          cu.user_id = $1::uuid
          AND c.id = $2
      `,
      values: [
        req.session.user_id,
        cart_id,
      ]
    });
  }
  catch(error) {
    console.error('query cart error', error);

    return res.status(500).json({
      status: 500,
      error: `Ошибка при получении корзины с ID ${cart_id}`
    });
  }

  if (select_cart_result.rows.length == 0) {
    return res.status(404).json({
      status: 404,
      error: `Ошибка при получении списка ${cart_id}`
    });
  }

  cart = select_cart_result.rows[0];

  // console.log('cart', cart);

  let select_cart_items_result = undefined;
  try {
    select_cart_items_result = await db.query({
      text: `
        SELECT *
        FROM carts_items
        WHERE cart_id = $1
      `,
      values: [
        cart_id,
      ]
    });

    // console.log(select_cart_items_result.rows);
  }
  catch(error) {
    console.error('query cart items error', error);

    return res.status(500).json({
      status: 500,
      error: `Ошибка при получении списка ${cart_id}`
    });
  }

  cart.items = select_cart_items_result.rows;

  let select_cart_users_result = undefined;
  try {
    select_cart_users_result = await db.query({
      text: `
        SELECT
          u.id AS user_id,
          p.id AS profile_id,
          p.first_name,
          p.last_name,
          p.sex
        FROM carts_users AS cu
        LEFT JOIN users AS u ON cu.user_id = u.id
        LEFT JOIN profiles AS p ON u.profile_id = p.id
        WHERE cu.cart_id = $1
      `,
      values: [
        cart_id,
      ]
    });

    // console.log(select_cart_users_result.rows);

    let cart_users = select_cart_users_result.rows;
    let users = [];

    for (let i = 0; i < cart_users.length; i++) {
      users.push({
        id: cart_users[i].user_id,
        profile: {
          id: cart_users[i].profile_id,
          first_name: cart_users[i].first_name,
          last_name: cart_users[i].last_name,
          sex: cart_users[i].sex,
        }
      });
    }

    cart.users = users;
  }
  catch(error) {
    console.error('query cart items error', error);

    return res.status(500).json({
      status: 500,
      error: `Ошибка при получении списка ${cart_id}`
    });
  }

  // db.query(
  //   {
  //     text: 'SELECT * FROM carts WHERE id = $1',
  //     values: [cart_id]
  //   },
  //   (error, result) => {
  //     if (error) {
  //       console.log('error', error);
  //       return res.status(400).json({
  //         status: 400,
  //         error: `Ошибка при получении корзины с ID ${cart_id}`
  //       });
  //     }

  //     let cart = result.rows[0];

  //     // console.log(result);

  //     db.query(
  //       {
  //         text: `SELECT * FROM carts_items WHERE cart_id = $1`,
  //         values: [cart_id]
  //       },
  //       (error, result) => {
  //         if (error) {
  //           console.log('error', error);
  //           return res.status(400).json({
  //             status: 400,
  //             error: `Ошибка при получении товаров для корзины с ID ${cart_id}`
  //           });
  //         }

  //         // Если записей нет, то отдаётся пустой массив.
  //         cart.items = result.rows;

  //         return res.status(200).json(cart);
  //       }
  //     );
  //   }
  // );

  return res.status(200).json(cart);
});

// Метод PUT меняет весь объект, включая ID
router.put('/api/carts/:cart_id', authorize, [
  param('cart_id').notEmpty().isInt().toInt(),
], async (req, res) => {
  let debug = true;

  const vResult = validationResult(req);
  if (! vResult.isEmpty()) {
    return res.status(400).send({ errors: vResult.array() });
  }

  let cart_id = req.params.cart_id;

  console.log(`PUT /api/carts/${cart_id}`);

  // console.log('req.body', req.body);

  // let result = undefined;

  // PG pool
  let cart = await Cart.findById(cart_id);

  console.log('cart:', cart.toObject());

  if (cart === null) {
    return res.status(403).json({
      status: 403,
      error: `Корзина с ID ${cart_id} не найдена`
    });
  }

  // console.log('db', db); // db - BoundPool

  let DbClient = await db.connect();
  // console.log('DbClient', DbClient); // DbClient - Client

  // Берём клиента из пула. Его надо будет обязательно вернуть в пул.
  cart.setDbClient(DbClient);

  debug && console.log('ТРАНЗАКЦИЯ: начало.');
  await DbClient.query('BEGIN');

  // PG client
  let cartUser = await cart.findCartUser(req.session.user_id);

  if (cartUser === null) {
    debug && console.log('ТРАНЗАКЦИЯ: откат.');
    await DbClient.query('ROLLBACK');
    debug && console.log('Возвращаю клиента в пул.');
    DbClient.release();

    return res.status(403).json({
      status: 403,
      error: `Корзина с ID ${cart_id} не связана с пользователем ${req.session.user_id}`
    });
  }
  
  let cart_has_changes = false;
  if ('name' in req.body) {
    cart.setName(req.body.name);
    cart_has_changes = true;
  }

  if ('items' in req.body) {
    let items = req.body.items;

    for (let i = 0; i < items.length; i++) {
      let item = items[i];

      console.log('item', item);
      // item {
      //   id: 10,
      //   cart_id: 16,
      //   name: 'Хлеб',
      //   quantity_plan: '1',
      //   quantity_fact: '0',
      //   picked: false
      // }

      let cartItem = null;

      if (
        'id' in item
        && item.id != 0
      ) {
        console.log('Элемент указан и не равен нулю');
        // PG pool
        cartItem = await CartItem.findById(item.id);

        if (cartItem === null) {
          debug && console.log('ТРАНЗАКЦИЯ: откат.');
          await DbClient.query('ROLLBACK');
          debug && console.log('Возвращаю клиента в пул.');
          DbClient.release();

          return res.status(404).json({
            status: 404,
            error: `Элемент ${item.id} в корзине ${cart_id} не найден`
          });
        }

        console.log('Найденный элемент', cartItem.toObject());
      }
      else {
        debug && console.log('Элемент не указан или равен нулю');
        cartItem = new CartItem();
        cartItem.setCartId(cart_id);
      }

      cartItem.setDbClient(DbClient);

      cartItem.setName(item.name);
      cartItem.setQuantityPlan(item.quantity_plan);
      cartItem.setQuantityFact(item.quantity_fact);
      //TODO: исправить. Везде использовать без is
      item.picked || item.is_picked ? cartItem.setPicked() : cartItem.setNotPicked();

      debug && console.log('cartItem перед сохранением', cartItem.toObject());

      let cart_item_save_result = await cartItem.save();

      if (cart_item_save_result === null) {
        debug && console.log('ТРАНЗАКЦИЯ: откат.');
        await DbClient.query('ROLLBACK');
        debug && console.log('Возвращаю клиента в пул.');
        DbClient.release();

        return res.status(500).json({
          status: 500,
          error: `При сохранении элемента ${cartItem.getName()} корзины ${cart_id} произошла ошибка`
        });
      }
    }
  }

  if (cart_has_changes) {
    await cart.save();
  }

  let response_json_string = await cart.toExtendedObject();
  
  debug && console.log('ТРАНЗАКЦИЯ: фиксация.');
  await DbClient.query('COMMIT');
  debug && console.log('Возвращаю клиента в пул.');
  DbClient.release();

  debug && console.log('response_json_string', response_json_string);

  return res.status(200).json(response_json_string);
});

router.patch('/api/carts/:id', authorize, [
  param('id').trim().isDecimal(),
  body('name').trim().notEmpty(),
], async (req, res) => {
  const result = validationResult(req);
  if (! result.isEmpty()) {
    return res.status(400).send({ errors: result.array() });
  }

  const id = req.params.id;
  const name = req.body.name;

  console.log(`PATCH /api/carts/${id}`);

  const cart = await Cart.findById(id);
  if (! cart) {
    return res.status(400).json({
      status: 400,
      error: `Ошибка при обновлении корзины с ID ${id}`
    });
  }

  cart.setName(name);

  try {
    await cart.update();
    // await cart.saveErr(); // Выкидывает TypeError: cart.saveErr is not a function
  }
  catch(error) {
    console.log('error', error);
    return res.status(400).json({
      status: 400,
      error: `Ошибка при обновлении корзины с ID ${id}`
    });
  }

  return res
    .status(200)
    .json(cart.toObject());
});

router.delete('/api/carts/:cart_id', authorize, async (req, res) => {
  let cart_id = req.params.cart_id;

  console.log(`DELETE /api/carts/${cart_id}`);

  let result = undefined;

  try {
    result = await db.query({
      text: 'DELETE FROM carts WHERE id = $1 RETURNING *',
      values: [cart_id]
    });
  }
  catch(error) {
    console.error('error', error);

    return res.status(400).json({
      status: 400,
      error: `Ошибка при удалении корзины с ID ${cart_id}`
    });
  }

  let cart = result.rows[0];

  res.status(200).json(cart);
});

// Элементы списка

router.post('/api/carts/:cart_id/items', authorize, [
  param('cart_id').notEmpty().isInt().toInt(),
], async (req, res) => {
  const _result = validationResult(req);
  if (! _result.isEmpty()) {
    return res.status(400).send({ errors: _result.array() });
  }

  let { cart_id } = req.params;

  console.log(`POST /api/carts/${cart_id}/items`);

  if (! 'name' in req.body) {
    // DbClient.release();

    return res.status(400).json({
      status: 400,
      error: "Не указано название товара"
    });
  }

  let DbClient = await db.connect();

  let cartItem = new CartItem();
  cartItem.setDbClient(DbClient);

  cartItem.setCartId(cart_id);
  cartItem.setName(req.body.name);

  if (! 'quantity_plan' in req.body) {
    DbClient.release();

    return res.status(400).json({
      status: 400,
      error: "Не указано планируемое количество товара"
    });
  }

  cartItem.setQuantityPlan(req.body.quantity_plan);

  let result = await cartItem.insert();

  if (result === null) {
    DbClient.release();

    return res.status(500).json({
      status: 500,
      error: `Ошибка при добавлении товара в корзину с ID ${cart_id}`
    });
  }

  DbClient.release();

  return res.status(200).json(result);
});

router.patch('/api/carts/:cart_id/items/:item_id', authorize, [
  param('cart_id').notEmpty().isInt().toInt(),
  param('item_id').notEmpty().isInt().toInt(),
], async (req, res) => {
  const _result = validationResult(req);
  if (! _result.isEmpty()) {
    return res.status(400).send({ errors: _result.array() });
  }

  let { cart_id, item_id } = req.params;

  console.log(`PATCH /api/carts/${cart_id}/items/${item_id}`);

  // PG pool
  let cartItem = await CartItem.findById(item_id);

  if (cartItem === null) {
    return res.status(400).json({
      status: 400,
      error: `Товар с ID ${item_id} в корзине с ID ${cart_id} не найден`
    });
  }

  if (cartItem.getCartId() !== cart_id) {
    return res.status(400).json({
      status: 400,
      error: `Товар с ID ${item_id} найден, но к корзине с ID ${cart_id} не относится`
    });
  }

  if ("name" in req.body) {
    cartItem.setName(req.body.name);
  }

  if ('quantity_plan' in req.body) {
    cartItem.setQuantityPlan(req.body.quantity_plan);
  }

  if ('picked' in req.body) {
    req.body.picked === true ? cartItem.setPicked() : cartItem.setNotPicked();
  }

  let DbClient = await db.connect();
  cartItem.setDbClient(DbClient);

  let result = await cartItem.update();

  if (result === null) {
    DbClient.release();

    return res.status(500).json({
      status: 500,
      error: `При обновлении товара с ID ${item_id} в корзине с ID ${cart_id} возникла непредвиденная ошибка`
    });
  }

  DbClient.release();

  return res.status(200).json(result);
});

router.delete('/api/carts/:cart_id/items/:item_id', authorize, [
  param('cart_id').notEmpty().isInt().toInt(),
  param('item_id').notEmpty().isInt().toInt(),
], async (req, res) => {
  const _result = validationResult(req);
  if (! _result.isEmpty()) {
    return res.status(400).send({ errors: _result.array() });
  }

  let { cart_id, item_id } = req.params;

  console.log(`DELETE /api/carts/${cart_id}/items/${item_id}`);

  let cartItem = await CartItem.findById(item_id);

  if (cartItem === null) {
    return res.status(400).json({
      status: 400,
      error: `Товар с ID ${item_id} не найден`
    });
  }

  if (cartItem.getCartId() !== cart_id) {
    return res.status(400).json({
      status: 400,
      error: `Товар с ID ${item_id} найден, но к корзине с ID ${cart_id} не относится`
    });
  }

  let DbClient = await db.connect();
  cartItem.setDbClient(DbClient);

  let result = await cartItem.delete();

  if (result === null) {
    DbClient.release();

    return res.status(500).json({
      status: 500,
      error: `Ошибка при удаления товара с ID ${item_id} в корзине с ID ${cart_id}`
    });
  }

  DbClient.release();

  return res.status(200).json(result);
});

// Пользователи списка

router.post('/api/carts/:cart_id/users', authorize, async (req, res) => {
  let { cart_id } = req.params;

  console.log(`POST /api/carts/${cart_id}/users`);

  if (! cart_id) {
    return res.status(400).json({
      status: 400,
      error: "Не указан ID списка"
    });
  }

  let owner_id = req.session.user_id;
  let candidate_id = req.body.user_id;

  let cart = await Cart.findById(cart_id);

  let DbClient = await db.connect();
  cart.setDbClient(DbClient);

  let ownerCartUser = await cart.findCartUser(owner_id);

  if (ownerCartUser === null) {
    DbClient.release();

    return res.status(400).json({
      status: 400,
      error: "Вы не пользователь списка"
    });
  }

  if (! ownerCartUser.isOwner()) {
    DbClient.release();

    return res.status(400).json({
      status: 400,
      error: "Вы не имеете права давать пользователю доступ к списку"
    });
  }

  let candidateCartUser = await cart.findCartUser(candidate_id);

  if (candidateCartUser !== null) {
    DbClient.release();

    return res.status(400).json({
      status: 400,
      error: `Добавляемый пользователь ${candidate_id} уже имеет доступ к списку`,
    });
  }

  candidateCartUser = new CartUser({
    cart_id: cart_id,
    user_id: candidate_id,
    is_owner: req.body.is_owner,
  });

  candidateCartUser.setDbClient(DbClient);

  let result_insert = await candidateCartUser.insert();

  if (result_insert === null) {
    DbClient.release();

    return res.status(400).json({
      status: 400,
      error: `При сохранении привязки пользователя ${candidate_id} к списку ${cart_id} произошла ошибка`,
    });
  }

  DbClient.release();

  return res.status(200).json(result_insert);
});

router.patch('/api/carts/:cart_id/users/:user_id', authorize, async (req, res) => {
  let { cart_id, user_id } = req.params;

  console.log(`PATCH /api/carts/${cart_id}/users/${user_id}`);

  if (! cart_id) {
    return res.status(400).json({
      status: 400,
      error: "Не указан ID списка"
    });
  }

  let owner_id = req.session.user_id;
  let candidate_id = req.params.user_id;

  if (owner_id === candidate_id) {
    return res.status(400).json({
      status: 400,
      error: "Вы не можете изменить свои собственные права"
    });
  }

  let cart = await Cart.findById(cart_id);
  // console.log('cart', cart.toObject());

  let ownerCartUser = await cart.findCartUser(owner_id);

  if (ownerCartUser === null) {
    return res.status(400).json({
      status: 400,
      error: "Вы не пользователь списка"
    });
  }

  if (! ownerCartUser.isOwner()) {
    return res.status(400).json({
      status: 400,
      error: "Вы не имеете права изменять права пользователей списка"
    });
  }

  let candidateCartUser = await cart.findCartUser(candidate_id);

  if (candidateCartUser === null) {
    return res.status(400).json({
      status: 400,
      error: `Пользователь ${candidate_id} не является пользователем списка`,
    });
  }

  let candidate_is_chaged = false;

  if ("is_owner" in req.body) {
    if (req.body.is_owner === candidateCartUser.isOwner()) {
      if (req.body.is_owner === true) {
        return res.status(400).json({
          status: 400,
          error: `Пользователь ${candidate_id} и так является владельцем списка`,
        });
      }
      else {
        return res.status(400).json({
          status: 400,
          error: `Пользователь ${candidate_id} и так не является владельцем списка`,
        });
      }
    }

    req.body.is_owner === true ? candidateCartUser.setIsOwner() : candidateCartUser.setNotOwner();
    candidate_is_chaged = true;
  }

  let update_result = null;
  if (candidate_is_chaged) {
    update_result = await candidateCartUser.update();
  }

  return res.status(200).json(update_result);
});

router.delete('/api/carts/:cart_id/users/:user_id', authorize, async (req, res) => {
  let { cart_id, user_id } = req.params;

  console.log(`DELETE /api/carts/${cart_id}/users/${user_id}`);

  if (! cart_id) {
    return res.status(400).json({
      status: 400,
      error: "Не указан ID списка"
    });
  }

  let owner_id = req.session.user_id;
  // console.log('owner_id', owner_id);
  let candidate_id = req.params.user_id;
  // console.log('candidate_id', candidate_id);

  // Найти пользователей по идентификатору списка.
  let select_cart_users_result = undefined;
  let cart_users = [];
  try {
    select_cart_users_result = await db.query({
      text: `
        SELECT
          cu.user_id,
          cu.is_owner
        FROM carts_users AS cu
        WHERE cu.cart_id = $1
      `,
      values: [
        cart_id,
      ]
    });

    // console.log(select_cart_users_result.rows);

    cart_users = select_cart_users_result.rows;
  }
  catch(error) {
    console.error('select cart users error', error);

    return res.status(500).json({
      status: 500,
      error: `Ошибка при получении пользователей списка ${cart_id}`
    });
  }

  if (cart_users.length == 0) {
    return res.status(404).json({
      status: 404,
      error: `Списка ${cart_id} не существует`
    });
  }

  // Проверить, что текущий пользователь относится к списку и он его владелец.
  let owner_founded = false;
  let candidate_founded = false;
  for (let i = 0; i < cart_users.length; i++) {
    if (
      cart_users[i].user_id == owner_id
      && cart_users[i].is_owner
    ) {
      owner_founded = true;
    }
    else if (cart_users[i].user_id == candidate_id) {
      candidate_founded = true;
    }
  }

  if (! owner_founded) {
    return res.status(403).json({
      status: 403,
      error: `Пользователю не разрешено добавлять других пользователей для редактирования списка`
    });
  }

  if (! candidate_founded) {
    return res.status(400).json({
      status: 400,
      error: `Удаляемый пользователь ${candidate_id} и так не имеет доступа к списку`
    });
  }

  let delete_cart_user_result = undefined;

  try {
    delete_cart_user_result = await db.query({
      text: `
        DELETE FROM carts_users
        WHERE
          cart_id = $1
          AND user_id = $2
        RETURNING *
      `,
      values: [
        cart_id,
        candidate_id,
      ]
    });
  }
  catch(error) {
    console.error('error', error);

    return res.status(400).json({
      status: 400,
      error: `Ошибка при удаления пользователя с ID ${candidate_id} корзини с ID ${cart_id}`
    });
  }

  return res.status(200).json(delete_cart_user_result.rows[0]);
});

module.exports = router;
