const { Router } = require('express');
const { query, check, validationResult } = require('express-validator');
// Валидатор проверяет body, cookies, headers, params, query
// check проверяет все эти типы сразу.
const crypto = require('crypto'); // шифрование одноразовых ключей PKCE
const randomstring = require('randomstring'); // создание случайной строки для PKCE
const axios = require('axios'); // обмен с внешними источниками данных
const pool = require('../db');
// const Credential = require('../models/Credential');
// const User = require('../models/User');
// Стандарт на возвращаемый JSON
// https://google.github.io/styleguide/jsoncstyleguide.xml

// CORS задаётся в server.js

const router = Router();

if (!process.env.CLIENT_ID) {
  console.log(
    'Не найдена переменная среды CLIENT_ID. Сервер не сможет получать токены пользователей. Выход.',
  );
  return;
}
const CLIENT_ID = process.env.CLIENT_ID;

if (!process.env.CLIENT_SECRET) {
  console.log(
    'Не найдена переменная среды CLIENT_SECRET. Сервер не сможет получать токены пользователей. Выход.',
  );
  return;
}
const CLIENT_SECRET = process.env.CLIENT_SECRET;

// URI – имя и адрес ресурса в сети, включает в себя URL и URN
// URL – адрес ресурса в сети, определяет местонахождение и способ обращения к нему
// URN – имя ресурса в сети, определяет только название ресурса, но не говорит как к нему подключиться

const FRONTEND_URL = process.env.PRODUCTION
  ? 'https://cart.universo.pro'
  : 'http://127.0.0.1:8080';
const BACKEND_URL = process.env.PRODUCTION
  ? FRONTEND_URL
  : `http://127.0.0.1:${process.env.APP_PORT || '3000'}`;

const REDIRECT_URI = `${BACKEND_URL}/universo/oauth/v2/callback`;

const USER_DATA_ENDPOINT = 'https://t34.universo.pro/api/v1.1/registrado/';

function authorize(req, res, next) {
  // console.log('req.session', req.session);
  if (req.session.auth && req.session.auth.access_token) {
    return next();
  }

  // res.redirect('/login');
  return res.status(403).json({
    error: {
      code: 403,
      message: 'Пользователь не авторизован',
    },
  });
}

router.get('/universo/oauth/v2/login', (req, res) => {
  console.log('GET /universo/oauth/v2/login');

  if (!req.session.auth) {
    req.session.auth = {
      pkce: {},
    };

    req.session.auth.pkce.code_verifier = randomstring
      .generate(128)
      .toString('base64url');
    req.session.auth.pkce.code_challenge = crypto
      .createHash('sha256')
      .update(req.session.auth.pkce.code_verifier)
      .digest('base64url');
  }

  if (req.session.auth.access_token) {
    return res.redirect(`${FRONTEND_URL}/?authenticated`);
  }

  let link = `https://t34.universo.pro/o/authorize/?response_type=code&code_challenge=${req.session.auth.pkce.code_challenge}&code_challenge_method=S256&client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URI}`;

  return res.redirect(link);
});

router.get('/universo/oauth/v2/logout', (req, res) => {
  console.log('GET /universo/oauth/v2/logout');
  delete req.session.auth;
  delete req.session.user; // TODO: проверить наличие и выпилить.
  delete req.session.user_id;

  return res.redirect(FRONTEND_URL);
});

// OAuth 2.0 for Browser-Based Apps
// https://datatracker.ietf.org/doc/html/draft-ietf-oauth-browser-based-apps#section-6.2

// Proof Key for Code Exchange (PKCE [RFC7636])

// Express преобразует путь в регулярное выражение. Можно использовать символы "+", "?", "*" "(" и ")".
// http://127.0.0.1:3000/auth/callback?code=qwe123
// http://127.0.0.1:3000/auth/callback

// https://express-validator.github.io/docs/guides/getting-started
// https://snyk.io/advisor/npm-package/express-validator/example

router.get( '/universo/oauth/v2/callback', [
  query('code', 'Нет кода авторизации').notEmpty()
], async (req, res) => {
  console.log('GET /universo/oauth/v2/callback');
  const result = validationResult(req);
  // if (result.isEmpty()) {
  //   return res.send(`Code is ${req.query.code}!`);
  // }
  // else {
  //   // console.log(result);
  //   // Result {
  //   //   formatter: [Function: formatter],
  //   //   errors: [
  //   //     {
  //   //       type: 'field',
  //   //       value: undefined,
  //   //       msg: 'Нет кода авторизации',
  //   //       path: 'code',
  //   //       location: 'query'
  //   //     }
  //   //   ]
  //   // }
  //   // return res.send(result.errors[0].msg);
  //   return res.json({
  //     error: {
  //       code: 400,
  //       message: 'Ошибка обработки обратного вызова',
  //       errors: result.errors
  //     }
  //   });
  // }

  const code = req.query.code;
  // console.log('code', code);

  let response;
  try {
    response = await axios.post('https://t34.universo.pro/o/token/',
      {
        grant_type: 'authorization_code',
        client_id: CLIENT_ID,
        client_secret: CLIENT_SECRET,
        code: code,
        code_verifier: req.session.auth.pkce.code_verifier || 'error_no_code',
        redirect_uri: REDIRECT_URI,
      },
      {
        headers: {
          'Cache-Control': 'no-cache',
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      },
    );
  }
  catch(error) {
    // console.log('CATCH', error.toJSON()); // AxiosError
    console.log('error', error);

    // При этом варианте получаем 504 Gateway Time-out
    // return res.status(error.response.status).json({
    //   error: {
    //     code: error.response.status,
    //     message: error.response.statusText,
    //   },
    // });

    return res.status(400).json({
      error: {
        code: 400,
        message: "При получении токена пользователя возникла проблема",
      },
    });
  }

  if (response.status != 200) {
    return res.status(response.status).json({
      error: {
        code: response.status,
        message: response.statusText,
      },
    });
  }
  req.session.auth.access_token = response.data.access_token;
  // console.log(req.session.auth.access_token);
  res.cookie('isLoggedin', true);

  const access_token = req.session.auth.access_token;

  let provider_user_response;
  try {
    provider_user_response = await axios.post(
      USER_DATA_ENDPOINT,
      {
        // operationName: "mi",
        query: `query {
          user: mi {
            id: objId
            first_name: unuaNomo {
              value: enhavo
            }
            middle_name: duaNomo {
              value: enhavo
            }
            last_name: familinomo {
              value: enhavo
            }
            avatar: avataro {
              id
              bildoE {
                url
              }
              bildoF {
                url
              }
            }
            sex: sekso
            cover:  kovrilo {
              id
              bildoBaza{url}
              bildoA{url}
              bildoB{url}
              bildoC{url}
              bildoD{url}
              bildoE{url}
              bildoF{url}
            }
          }
        }`,
      },
      {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${access_token}`,
        },
      },
    )
  }
  catch(error) {
    // console.log('response', error.response);
    return res.status(error.response.status).json({
      error: {
        code: error.response.status,
        message: error.response.statusText,
      },
    });
  }

  // console.log('user data', JSON.stringify(provider_user_response.data.data.user, null, 2));
  const provider_user_data = provider_user_response.data.data.user;

  // Создать подключение к БД
  const db_client = await pool.connect();
  try {
    // User.setDBClient(db_client);
    const query_select_credential = {
      text: `
        SELECT *
        FROM credentials
        WHERE
          provider_name = $1::text
          AND provider_user_id = $2::text
      `,
      values: [
        'universo.pro',
        provider_user_data.id
      ]
    };

    const result_select_credential = await db_client.query(query_select_credential); // Result
    // console.log('result_select_credential', result_select_credential);

    let profile_sex = 'u'; // unknown
    if (provider_user_data.sex) {
      if (provider_user_data.sex === 'vira') {
        profile_sex = 'm';
      }
      else if (provider_user_data.sex === 'virina') {
        profile_sex = 'f';
      }
    }
    
    // console.log('profile_sex', profile_sex);

    if (result_select_credential.rowCount === 1) {
      const credential_data = result_select_credential.rows[0];
      req.session.user_id = credential_data.user_id;
    }
    else if (result_select_credential.rowCount === 0) {
      try {
        await db_client.query('BEGIN');

        const query_insert_profile = {
          text: `
            INSERT INTO profiles  (id,      first_name, middle_name,  last_name,  sex,  created_at)
            VALUES                (DEFAULT, $1::text,   $2::text,     $3::text,   $4,   NOW()::timestamp(0))
            RETURNING *
          `,
          values: [
            provider_user_data.first_name.value,
            provider_user_data.middle_name ? provider_user_data.middle_name.value : '',
            provider_user_data.last_name.value,
            profile_sex,
          ]
        }

        const result_insert_profile = await db_client.query(query_insert_profile); // Result
        // console.log('result_insert_profile', result_insert_profile);

        const profile_data = result_insert_profile.rows[0];
        // console.log('profile_data', profile_data);

        const query_insert_user = {
          text: `
            INSERT INTO users (id, name, hashed_password, salt, profile_id, created_at)
            VALUES (DEFAULT, $1, '-', '-', $2, NOW()::timestamp(0))
            RETURNING *
          `,
          values: [
            provider_user_data.id,
            profile_data.id,
          ]
        }

        const result_insert_user = await db_client.query(query_insert_user); // Result
        // console.log('result_insert_user', result_insert_user);

        const user_data = result_insert_user.rows[0];
        // console.log('user_data', user_data);

        const query_insert_credential = {
          text: `
            INSERT INTO credentials (id, provider_name, provider_user_id, user_id, created_at)
            VALUES (DEFAULT, $1::text, $2::text, $3, NOW()::timestamp(0))
            RETURNING *
          `,
          values: [
            'universo.pro',
            provider_user_data.id,
            user_data.id,
          ]
        }

        const result_insert_credential = await db_client.query(query_insert_credential); // Result
        // console.log('result_insert_credential', result_insert_credential);

        const credential_data = result_insert_credential.rows[0];
        // console.log('credential_data', credential_data);

        req.session.user_id = credential_data.user_id;

        await db_client.query('COMMIT');
      }
      catch(error) {
        await db_client.query('ROLLBACK');
        console.log('ROLLBACK');
        console.log('error', error);
      }
    }
  }
  catch (e) {
    // await db_client.query('ROLLBACK');
    // console.log('ROLLBACK');
    console.log('error', e);
  //   throw e
  }
  finally {
    db_client.release();
  }

  // .then((response) => {
  //   console.log(JSON.stringify(response.data, null, 2));

  //   res.json({
  //     id: response.data.data.user.id,
  //     first_name: response.data.data.user.first_name.value,
  //     last_name: response.data.data.user.last_name.value,
  //     avatar: response.data.data.user.avatar,
  //     cover: response.data.data.user.cover,
  //     //пол приходит на эсперанто vira virina, потому тут перевожу
  //     sex:
  //       response.data.data.user.sex.localeCompare('vira', undefined, {
  //         sensitivity: 'base',
  //       }) === 0
  //         ? 'male'
  //         : response.data.data.user.sex.localeCompare('virina', undefined, {
  //             sensitivity: 'base',
  //           }) === 0
  //         ? 'female'
  //         : 'ufo',
  //   });

  //   // res.send(`<a href=\'/logout'>logout</a><br>User data SUCCESS`);
  //   // res.render(
  //   //   'auth/user',
  //   //   {
  //   //     user: {
  //   //       id: response.data.data.user.id,
  //   //       first_name: response.data.data.user.first_name.value,
  //   //       last_name: response.data.data.user.last_name.value
  //   //     }
  //   //   }
  //   // );
  // })
  // .catch((error) => {
  //   if (error.response) {
  //     console.log(error.response.data);
  //     console.log(error.response.status);
  //     console.log(error.response.headers);
  //   } else if (error.request) {
  //     console.log(error.request);
  //   } else {
  //     console.log('Error', error.message);
  //   }
  //   // console.log(error.config);

  //   return res.json({
  //     error: {
  //       code: 'ERR_NO_USER',
  //       message: 'Данные по пользователю не получены',
  //     },
  //   });
  // });

  // const credential = Credential.findOneById();

  return res.redirect(`${FRONTEND_URL}/?authenticated`);

  // axios
  //   .post(
  //     'https://t34.universo.pro/o/token/',
  //     {
  //       grant_type: 'authorization_code',
  //       client_id: CLIENT_ID,
  //       client_secret: CLIENT_SECRET,
  //       code: code,
  //       code_verifier: req.session.auth.pkce.code_verifier || 'error_no_code',
  //       redirect_uri: REDIRECT_URI,
  //     },
  //     {
  //       headers: {
  //         'Cache-Control': 'no-cache',
  //         'Content-Type': 'application/x-www-form-urlencoded',
  //       },
  //     },
  //   )
  //   .then((response) => {
  //     req.session.auth.access_token = response.data.access_token;
  //     console.log(req.session.auth.access_token);
  //     res.cookie('isLoggedin', true);
  //     console.log('req.session');
  //     console.log(req.session);

  //     // const credential = Credential.findOneById();

  //     return res.redirect(`${FRONTEND_URL}/?authenticated`);
  //   })
  //   .catch((error) => {
  //     if (error.response) {
  //       // The request was made and the server responded with a status code
  //       // that falls out of the range of 2xx
  //       console.log('error.response.data');
  //       console.log(error.response.data);
  //       console.log('error.response.status');
  //       console.log(error.response.status);
  //       console.log('error.response.headers');
  //       console.log(error.response.headers);
  //     } else if (error.request) {
  //       // The request was made but no response was received
  //       // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
  //       // http.ClientRequest in node.js
  //       console.log('error.request');
  //       console.log(error.request);
  //     } else {
  //       // Something happened in setting up the request that triggered an Error
  //       console.log('Error', error.message);
  //     }
  //     console.log('error.config');
  //     console.log(error.config);
  //     return res.send('CATCH');
  //   });
});

router.get('/user', authorize, (req, res) => {
  if (!req.session.auth) {
    return res.json({
      error: {
        code: 'ERR_NO_USER',
        message: 'Нет данных по текущему пользователю',
      },
    });
  }
  const access_token = req.session.auth.access_token;

  axios.post(
      USER_DATA_ENDPOINT,
      {
        // operationName: "mi",
        query: `query {
        user: mi {
          id: objId
          first_name: unuaNomo {
            value: enhavo
          }
          last_name: familinomo {
            value: enhavo
          }
          avatar: avataro {
            id
            bildoE {
              url
            }
            bildoF {
              url
            }
          }
          sex: sekso
          cover:  kovrilo {
            id
            bildoBaza{url}
            bildoA{url}
            bildoB{url}
            bildoC{url}
            bildoD{url}
            bildoE{url}
            bildoF{url}
          }
        }
      }`,
      },
      {
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${access_token}`,
        },
      },
    )
    .then((response) => {
      // console.log('Universo user', JSON.stringify(response.data, null, 2));

      let sex = 'ufo';
      if (response.data.data.user.sex !== null) {
        // Пол приходит на эсперанто - vira или virina, потому тут перевожу
        switch(response.data.data.user.sex.toLowerCase()) {
          case 'vira':
            sex = 'male';
            break;
          case 'virina':
            sex = 'female';
            break;
        }
      }

      res.json({
        id: response.data.data.user.id,
        first_name: response.data.data.user.first_name.value,
        last_name: response.data.data.user.last_name.value,
        avatar: response.data.data.user.avatar,
        cover: response.data.data.user.cover,
        sex: sex,
      });
    })
    .catch((error) => {
      if (error.response) {
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
      }
      else if (error.request) {
        console.log(error.request);
      }
      else {
        console.log('Error', error.message);
      }
      // console.log(error.config);

      return res.json({
        error: {
          code: 'ERR_NO_USER',
          message: 'Данные по пользователю не получены',
        },
      });
    });
});

// req.query
// Пример валидации
// https://www.tabnine.com/code/javascript/functions/express-validator/ValidationChain/isLength
router.get('/api/users', /* authorize, */ [
  query('find').trim().notEmpty().isLength({min: 3}).withMessage('Должно быть минимум три символа')
], async (req, res) => {
  console.log('GET /api/users');

  const vResult = validationResult(req);
  if (! vResult.isEmpty()) {
    return res.send({ errors: vResult.array() });
  }

  // Проверяем и очищаем полученное значение
  let find = req.query.find;

  let select_users_result = undefined;

  try {
    select_users_result = await pool.query({
      text: `
        SELECT
          u.id AS user_id,
          p.id AS profile_id,
          p.first_name,
          p.last_name,
          p.sex
        FROM users AS u
        LEFT JOIN profiles AS p ON p.id = u.profile_id
        WHERE
          lower(p.first_name) LIKE lower($1)
          OR lower(p.last_name) LIKE lower($1)
        LIMIT 5
      `,
      values: [
        `%${find}%`,
      ]
    });
  }
  catch(error) {
    console.error('query cart error', error);

    return res.status(500).json({
      status: 500,
      error: `Ошибка при поиске пользователей`
    });
  }

  let rows = select_users_result.rows;
  let users = [];

  for (let i = 0; i < rows.length; i++) {
    users.push({
      id: rows[i].user_id,
      profile: {
        id: rows[i].profile_id,
        first_name: rows[i].first_name,
        last_name: rows[i].last_name,
        sex: rows[i].sex,
      }
    });
  }

  return res.status(200).json(users);
});

module.exports = router;
