#!/bin/bash

# Цвет текста
NO_COLOR='\033[0m'
RED_COLOR='\033[0;31m'
# YELLOW_COLOR='\033[0;33m'
GREEN_COLOR='\033[0;32m'
# WHITE_BOLD_COLOR='\033[1;37m'

echo
echo "Начинаю выполнять скрипт по удалению СУБД."

if ! command -v docker &> /dev/null
then
    echo -e "${RED_COLOR}[ ERROR ]${NO_COLOR} Клиент Docker не найден."
    echo "Установите Docker на локальную машину."
    echo "Инструкции по установке вы можете найти здесь https://docs.docker.com/engine/install/"
    echo "Выход"
    exit 1
fi

echo -e "${GREEN_COLOR}[ OK ]${NO_COLOR} Клиент Docker обнаружен."

docker stop cup-db01p cup-cache01p
docker rm cup-db01p cup-cache01p
docker volume rm cup-postgresql-data-volume
docker volume rm cup-backup-volume
docker network rm cup-net1

[ -f ./sql/last ] && rm ./sql/last

echo -e "${GREEN_COLOR}[ OK ]${NO_COLOR} Скрипт удаления успешно завершил свою работу."
