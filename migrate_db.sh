#!/bin/bash

set -e

# Цвет текста
NO_COLOR='\033[0m'
RED_COLOR='\033[0;31m'
# YELLOW_COLOR='\033[0;33m'
GREEN_COLOR='\033[0;32m'
BLUE_COLOR='\033[0;34m'

echo
echo "Начинаю выполнять скрипт по миграции данных."

# export PGPASSWORD=secret

# Директория, в которой размещаются скрипты SQL
SQL_ROOT=./sql
# Переменная среды содержащая пароль к БД, которую psql ищет по умолчанию.
if [ ! -v PGPASSWORD ]
then
  export PGPASSWORD=secret
fi

if ! command -v psql &> /dev/null
then
    echo -e "${RED_COLOR}[ ERROR ]${NO_COLOR} Команда psql не найдена. Создание структуры и миграция данных невозможны. Выход."
    exit 1
fi

echo "Проверяю доступность СУБД для подключения."

# СУБД не сразу поднимается в контейнере. Требуется некоторое время.
PG_IS_READY=false
for i in {0..10}
do
  echo -ne "$((10 - i)) \r"
  if pg_isready -h "127.0.0.1" -U admin -d wms -q
  then
    PG_IS_READY=true
    break
  fi
  sleep 1
done

if $PG_IS_READY
then
  echo -e "${GREEN_COLOR}[ OK ]${NO_COLOR} СУБД доступна для подключения."
else
  echo -e "${RED_COLOR}[ ERROR ]${NO_COLOR} СУБД недоступна для подключения. Выход."
  exit 1
fi

# Страховка на случай если надо перечитать все скрипты SQL
# IMPORT_FILE=true

if [ -f $SQL_ROOT/last ]
then
  LAST_FILE=$(cat "$SQL_ROOT"/last)
  echo -e "${BLUE_COLOR}[ INFO ]${NO_COLOR} Найден файл last с именем файла последней миграции $LAST_FILE${N}"
  IMPORT_FILE=false
else
  IMPORT_FILE=true
fi

for FILE in "$SQL_ROOT"/*.sql
do
  if $IMPORT_FILE
  then
    echo "Импортирую файл $FILE в БД."
    # psql -h "127.0.0.1" -U admin -f "$FILE" -d wms
    # set -x;
    psql -h "127.0.0.1" -U admin --no-password wms < "$FILE"
    # echo "$PGPASSWORD"
    # psql -h "127.0.0.1" --username=admin --no-password wms -f "$FILE"
    # set +x;

    if [ $? -eq 0 ]
    then
      echo -e "${GREEN_COLOR}[ OK ]${NO_COLOR} SQL-скрипт из файла $FILE успешно импортирован."
    else
      echo -e "${RED_COLOR}[ ERROR ]${NO_COLOR} При импорте SQL-скрипта из файла $FILE произошёл сбой. Пропускаю."
    fi
  else
    echo -e "${BLUE_COLOR}[ INFO ]${NO_COLOR} Пропускаю файл $FILE"
    CURRENT_FILE=$(basename -- "$FILE")
    if [[ "$LAST_FILE" == "$CURRENT_FILE" ]]
    then
      IMPORT_FILE=true
    fi
  fi
done

echo -e "${BLUE_COLOR}[ INFO ]${NO_COLOR} Записываю имя последнего файла SQL в файл $SQL_ROOT/last${N}"
basename -- "$FILE" > "$SQL_ROOT"/last

echo -e "${GREEN_COLOR}[ OK ]${NO_COLOR} Миграция БД выполнена успешно.${N}"
exit 0
