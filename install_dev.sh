#!/bin/bash

# Цвет текста
NO_COLOR='\033[0m'
RED_COLOR='\033[0;31m'
YELLOW_COLOR='\033[0;33m'
GREEN_COLOR='\033[0;32m'
WHITE_BOLD_COLOR='\033[1;37m'

PREFIX=cup
DB_CONTAINER_NAME="${PREFIX}-db01p"
CACHE_CONTAINER_NAME="${PREFIX}-cache01p"

# if [ "$EUID" -ne 0 ]
#   then echo "Please run as root"
#   exit
# fi

# netstat -l | grep 5432 | wc -l

echo
echo "Начинаю выполнять скрипт по установке бекенда, СУБД и сервера кешей."

if ! command -v docker &> /dev/null
then
    echo -e "${RED_COLOR}[ ERROR ]${NO_COLOR} Клиент Docker не найден."
    echo "Установите Docker на локальную машину."
    echo "Инструкции по установке вы можете найти здесь https://docs.docker.com/engine/install/"
    echo "Выход"
    exit 1
fi

echo -e "${GREEN_COLOR}[ OK ]${NO_COLOR} Клиент Docker обнаружен."

USER_HAS_GROUP_DOCKER=false
for GROUP in $(groups)
do
  if [ docker = "$GROUP" ]
  then
    USER_HAS_GROUP_DOCKER=true
  fi
done

if ! $USER_HAS_GROUP_DOCKER
then
  echo -e "${RED_COLOR}[ ERROR ]${NO_COLOR} Пользователя нет в группе docker."
  echo "Выполните команду:"
  echo -e "${WHITE_BOLD_COLOR}sudo usermod -aG docker $USER${NO_COLOR}"
  echo "И повторите установку сначала."
  echo "Выход"
  exit 1
fi

echo -e "${GREEN_COLOR}[ OK ]${NO_COLOR} Пользователь $USER состоит в группе docker."

echo "Выполняю поиск контейнера с СУБД (PostgreSQL 15.3)."
if [ "$(docker ps -a -q -f name=${DB_CONTAINER_NAME} | wc -l)" -eq 1 ];
then
  echo -e "${YELLOW_COLOR}[ WARN ]${NO_COLOR} Контейнер ${DB_CONTAINER_NAME} уже существует."
  if [ "$(docker ps -q -f name=${DB_CONTAINER_NAME} | wc -l)" -eq 1 ];
  then
    echo -e "${YELLOW_COLOR}[ WARN ]${NO_COLOR} Контейнер ${DB_CONTAINER_NAME} уже запущен."
  else
    echo -e "${YELLOW_COLOR}[ WARN ]${NO_COLOR} Контейнер ${DB_CONTAINER_NAME} не запущен."
    echo "Запускаю контейнер ${DB_CONTAINER_NAME}"
    if [ "$(docker start ${DB_CONTAINER_NAME})" ];
    then
      echo -e "${GREEN_COLOR}[ OK ]${NO_COLOR} Контейнер ${DB_CONTAINER_NAME} запущен."
    else
      echo -e "${RED_COLOR}[ ERROR ]${NO_COLOR} Контейнер ${DB_CONTAINER_NAME} не запущен. Выход."
      exit 1
    fi
  fi
else
  if [ "$(docker network ls -q -f name=cup-net1 | wc -l)" -eq 1 ];
  then
    echo -e "${YELLOW_COLOR}[ WARN ]${NO_COLOR} Сеть cup-net1 уже существует. Пропускаю."
  else
    echo "Добавляю в Docker сеть cup-net1"
    docker network create -d bridge cup-net1
    if [ $? ]
    then
      echo -e "${GREEN_COLOR}[ OK ]${NO_COLOR} Сеть cup-net1 добавлена."
    else
      echo -e "${RED_COLOR}[ ERROR ]${NO_COLOR} Сеть cup-net1 не добавлена. Выход."
      exit 1
    fi
  fi

  if [ "$(docker volume ls -q -f name=cup-postgresql-data-volume | wc -l)" -eq 1 ];
  then
    echo -e "${YELLOW_COLOR}[ WARN ]${NO_COLOR} Хранилище cup-postgresql-data-volume уже существует. Пропускаю."
  else
    echo "Добавляю в Docker хранилище cup-postgresql-data-volume"
    docker volume create cup-postgresql-data-volume
    if [ $? ]
    then
      echo -e "${GREEN_COLOR}[ OK ]${NO_COLOR} Хранилище cup-postgresql-data-volume добавлено."
    else
      echo -e "${RED_COLOR}[ ERROR ]${NO_COLOR} Хранилище cup-postgresql-data-volume не добавлено. Выход."
      exit 1
    fi
  fi

  if [ "$(docker volume ls -q -f name=cup-backup-volume | wc -l)" -eq 1 ];
  then
    echo -e "${YELLOW_COLOR}[ WARN ]${NO_COLOR} Хранилище cup-backup-volume уже существует. Пропускаю."
  else
    echo "Добавляю в Docker хранилище cup-backup-volume"
    docker volume create cup-backup-volume
    if [ $? ]
    then
      echo -e "${GREEN_COLOR}[ OK ]${NO_COLOR} Хранилище cup-backup-volume добавлено."
    else
      echo -e "${RED_COLOR}[ ERROR ]${NO_COLOR} Хранилище cup-backup-volume не добавлено. Выход."
      exit 1
    fi
  fi

  if [ -z "$POSTGRES_PASSWORD" ]
  then
    export POSTGRES_PASSWORD=secret
  fi

  docker run \
    --name cup-db01p \
    --volume cup-postgresql-data-volume:/var/lib/postgresql/data \
    --volume cup-backup-volume:/backup \
    --env POSTGRES_DB=wms \
    --env POSTGRES_USER=admin \
    --env POSTGRES_PASSWORD="$POSTGRES_PASSWORD" \
    --network cup-net1 \
    --publish 127.0.0.1:5432:5432 \
    --detach \
    postgres:15.3-alpine3.18

  echo "Статус запуска контейнера $?"

  # if [ ! $? -eq 0 ]
  # then
  #   echo -e "${GREEN_COLOR}[ OK ]${NO_COLOR} Контейнер $DB_CONTAINER_NAME запущен."
  # else
  #   echo -e "${RED_COLOR}[ ERROR ]${NO_COLOR} Контейнер $DB_CONTAINER_NAME не запущен. Выход."
  #   exit 1
  # fi
fi

echo -e "${GREEN_COLOR}[ OK ]${NO_COLOR} Контейнер с БД готов к работе."

echo "Выполняю поиск контейнера с кешем сессий (Redis 7.2.3)."
if [ "$(docker ps -a -q -f name=${CACHE_CONTAINER_NAME} | wc -l)" -eq 1 ];
then
  echo -e "${YELLOW_COLOR}[ WARN ]${NO_COLOR} Контейнер ${CACHE_CONTAINER_NAME} уже существует."
else
  docker run \
    --name cup-cache01p \
    --network cup-net1 \
    --publish 127.0.0.1:6379:6379 \
    --detach \
    redis:7.2.3-alpine3.19
fi

echo -e "${GREEN_COLOR}[ OK ]${NO_COLOR} Контейнер с кешем сессий готов к работе."

# Требуется время на запуск СУБД в контейнере.
# Миграция данных
bash migrate_db.sh

# docker volume ls -q -f name=cup-postgresql-data-volume
# docker volume ls -q -f name=cup-backup-volume
# sudo docker volume create cup-postgresql-data-volume
# sudo docker volume create cup-backup-volume

# npm install &>npm_install.log

echo "Устанавливаю пакеты NPM."
if ! npm install &>npm_install.log;
then
  echo -e "${RED_COLOR}[ ERROR ]${NO_COLOR} При установке пакетов через NPM произошла ошибка. Смотрите выше. Выход."
  exit 1
fi

echo -e "${GREEN_COLOR}[ OK ]${NO_COLOR} Пакеты NPM установлены."

if [ ! -f .env ];
then
  echo -e "${YELLOW_COLOR}[ WARN ]${NO_COLOR} Создаю файл .env с переменными среды для сервера."

  {
    echo "APP_PORT='3000'"
    echo
    echo "PGHOST='127.0.0.1'"
    echo PGPORT=5432
    echo "PGDATABASE='wms'"
    echo "PGUSER='admin'"
    echo "PGPASSWORD='secret'"
    echo
  } > .env
  read -r -p "Введите идентификатор клиента (CLIENT_ID) OAuth2 для сервера: " CLIENT_ID
  echo "CLIENT_ID='$CLIENT_ID'" >> .env
  read -r -p "Введите секретный ключ клиента (CLIENT_SECRET) OAuth2 для сервера: " CLIENT_SECRET
  echo "CLIENT_SECRET='$CLIENT_SECRET'" >> .env

  echo -e "${GREEN_COLOR}[ OK ]${NO_COLOR} Файл .env с переменными среды создан."
fi

echo -e "${GREEN_COLOR}[ OK ]${NO_COLOR} Скрипт установки успешно завершил свою работу."

echo "Для запуска сервера в режиме разработки выполните команду:"
echo -e "${WHITE_BOLD_COLOR}npm run dev${NO_COLOR}"

exit 0
