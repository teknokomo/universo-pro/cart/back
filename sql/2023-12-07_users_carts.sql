-- Изменение ограничений для внешних ключей для возможности удаления корзин без предварительного удаления привязок пользователей к корзинам.

ALTER TABLE users_carts
DROP CONSTRAINT users_carts_user_id_fkey,
DROP CONSTRAINT users_carts_cart_id_fkey,
ADD CONSTRAINT users_carts_user_id_fkey
  FOREIGN KEY (user_id)
  REFERENCES users (id)
  ON DELETE CASCADE,
ADD CONSTRAINT users_carts_cart_id_fkey
  FOREIGN KEY (cart_id)
  REFERENCES carts (id)
  ON DELETE CASCADE;
