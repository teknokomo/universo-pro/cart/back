-- Переименование таблицы users_carts в carts_users

ALTER TABLE IF EXISTS users_carts
RENAME TO carts_users;

-- Формирование однотипного именования группы таблиц связанных по смыслу
-- carts        - хранит информацию о корзинах
-- carts_items  - хранит информацию об элементах корзин
-- carts_users  - хранит информацию о пользователях корзины
