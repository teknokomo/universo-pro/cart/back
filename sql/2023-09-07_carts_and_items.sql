-- Лучшие практики проектирования баз данных PostgreSQL
-- https://appmaster.io/ru/blog/luchshie-praktiki-proektirovaniia-baz-dannykh-postgresql
-- https://blog.logrocket.com/crud-rest-api-node-js-express-postgresql/

CREATE TABLE IF NOT EXISTS public.carts(
  id SERIAL PRIMARY KEY,
  name VARCHAR(255)
);

-- Если создавать повторно, то получаю ошибку
-- ERROR:  relation "carts" already exists

CREATE TABLE IF NOT EXISTS public.carts_items(
  id SERIAL PRIMARY KEY,
  cart_id INTEGER,
  name VARCHAR(255),
  quantity_plan NUMERIC,
  quantity_fact NUMERIC,
  picked BOOLEAN,
  FOREIGN KEY (cart_id) REFERENCES carts (id) ON DELETE CASCADE
);

-- Если не указывать ON DELETE CASCADE, то корзина, к которой привязаны товары НЕ удалится.
-- Будет получена ошибка:
-- code: '23503',
-- detail: 'Key (id)=(2) is still referenced from table "carts_items".'

-- Если создавать повторно, то получаю ошибку
-- ERROR:  relation "carts_items" already exists

-------------
-- ПРИМЕРЫ --
-------------

-- Пример добавления записи корзины
-- INSERT
-- INTO carts (name)
-- VALUES
--   ('Имя новой корзины');

-- Пример добавления записи товара в корзину
-- INSERT
-- INTO carts (name)
-- VALUES
--   ('Имя новой корзины');

-- Удаление внешнего ключа
-- ALTER TABLE public.carts_items
-- DELETE constraint