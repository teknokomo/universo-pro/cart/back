-- Таблица аутентификаций на серверах аутентификации OAuth
CREATE TABLE IF NOT EXISTS public.credentials(
  id uuid DEFAULT gen_random_uuid() PRIMARY KEY,
  provider_name VARCHAR(255) NOT NULL,
  provider_user_id VARCHAR(255) NOT NULL,
  user_id uuid NOT NULL,
  created_at TIMESTAMP DEFAULT NOW(),
  FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE
);
