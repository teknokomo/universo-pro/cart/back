
-- Изменение состава колонок
-- Было:
--  id | email | hashed_password | salt | first_name | last_name | sex | creation_date | is_confirmed | confirmation_code
-- Станет:
--  id | name | hashed_password | salt | profile_id | created_at

DO $$
DECLARE
  v_row_column_data information_schema.columns%rowtype;

  v_table_schema VARCHAR = 'public';
  v_table_name VARCHAR = 'users';
BEGIN
  RAISE NOTICE 'Ищу колонку email в таблице % схемы %.', v_table_name, v_table_schema;

  SELECT *
  FROM information_schema.columns
  INTO v_row_column_data
  WHERE
    table_schema = v_table_schema
    AND table_name = v_table_name
    AND column_name = 'email';
  
  IF FOUND THEN
    RAISE NOTICE 'Колонка email в таблице % найдена. Переименовываю.', v_table_name;

    ALTER TABLE public.users
    RENAME COLUMN email TO name;
  ELSE
    RAISE NOTICE 'Колонка email в таблице % не найдена. Ничего переименовывать не надо. Пропускаю.', v_table_name;
  END IF;


  RAISE NOTICE 'Удаляю колонки first_name, last_name, sex, is_confirmed, confirmation_code.';

  ALTER TABLE IF EXISTS public.users
  DROP COLUMN IF EXISTS first_name,
  DROP COLUMN IF EXISTS last_name,
  DROP COLUMN IF EXISTS sex,
  DROP COLUMN IF EXISTS is_confirmed,
  DROP COLUMN IF EXISTS confirmation_code;


  RAISE NOTICE 'Добавляю в таблицу users колонки profile_id, created_at.';

  ALTER TABLE IF EXISTS public.users
  ADD COLUMN IF NOT EXISTS profile_id uuid REFERENCES profiles (id) ON DELETE RESTRICT,
  ADD COLUMN IF NOT EXISTS created_at TIMESTAMP DEFAULT NOW();
  

  RAISE NOTICE 'Ищу колонку creation_date в таблице % схемы %.', v_table_name, v_table_schema;

  SELECT *
  FROM information_schema.columns
  INTO v_row_column_data
  WHERE
    table_schema = v_table_schema
    AND table_name = v_table_name
    AND column_name = 'creation_date';
  
  IF FOUND THEN
    RAISE NOTICE 'Колонка creation_date в таблице % найдена. Переношу данные в колонку created_at.', v_table_name;

    UPDATE public.users
    SET created_at = creation_date;

    RAISE NOTICE 'Удаляю колонку creation_date из таблицы %.', v_table_name;

    ALTER TABLE IF EXISTS public.users
    DROP COLUMN IF EXISTS creation_date;
  ELSE
    RAISE NOTICE 'Колонка creation_date в таблице % не найдена. Перенос данных не требуется. Пропускаю.', v_table_name;
  END IF;
END $$;
