-- Пользователем является тот, кто может войти в систему и использовать весь или часть её функционала.

-- Создание таблицы для пользователей
CREATE TABLE IF NOT EXISTS public.users(
  id uuid DEFAULT gen_random_uuid() PRIMARY KEY,
  email VARCHAR(255) NOT NULL,
  hashed_password VARCHAR(255) NOT NULL,
  salt VARCHAR(255) NOT NULL,
  first_name VARCHAR(255),
  middle_name VARCHAR(255),
  last_name VARCHAR(255),
  sex VARCHAR(1),
  created_at TIMESTAMP DEFAULT NOW()
);

-- https://blog.codinghorror.com/youre-probably-storing-passwords-incorrectly/

-- Основные типы
-- varchar(n) - текст переменной длины с ограничением
-- char(n) - текст постоянной длины с ограничением
-- text - текст переменной длины без ограничений
-- boolean



-- Проверка реакции на сбой.
-- Данный запрос не должен быть выполнен, так как таблица carts имеет связанную с ней таблицу carts_items.
-- DROP TABLE IF EXISTS carts;

-- Получено сообщение об ошибке:
-- psql:./sql/2023-09-26_empty.sql:3: ERROR:  cannot drop table carts because other objects depend on it
-- DETAIL:  constraint carts_items_cart_id_fkey on table carts_items depends on table carts
-- HINT:  Use DROP ... CASCADE to drop the dependent objects too.
