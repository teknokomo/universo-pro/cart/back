-- Привязка пользователей к корзинам

CREATE TABLE IF NOT EXISTS users_carts (
  user_id uuid REFERENCES users (id) ON DELETE RESTRICT,
  cart_id integer REFERENCES carts (id) ON DELETE RESTRICT,
  is_owner boolean DEFAULT 'false',
  PRIMARY KEY (user_id, cart_id)
);
