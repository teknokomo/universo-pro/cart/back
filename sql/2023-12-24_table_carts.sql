-- Добавление поля  для идентификатора копии записи.

ALTER TABLE  IF EXISTS public.carts
ADD COLUMN IF NOT EXISTS replica_id INTEGER DEFAULT 0;
