-- Пользователь должен послать другому пользователю приглашение, чтобы приглашённый мог подключиться к его списку.

CREATE TABLE IF NOT EXISTS users_carts_invitations (
  id uuid DEFAULT gen_random_uuid() PRIMARY KEY,
  sender_id uuid REFERENCES users (id) ON DELETE RESTRICT,
  recipient_id uuid REFERENCES users (id) ON DELETE RESTRICT,
  cart_id INTEGER  REFERENCES carts (id) ON DELETE RESTRICT,
  status VARCHAR(64) NOT NULL,
  created_at TIMESTAMP,
  finished_at TIMESTAMP
);

-- колонка status:
-- sended - отправлено отправителем
-- revoked - отозвано отправителем
-- accepted - принято получателем
-- rejected - отклонено получателем
