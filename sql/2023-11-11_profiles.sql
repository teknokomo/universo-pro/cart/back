-- Профили содержат данные людей, которые могут и не быть пользователями системы.

CREATE TABLE IF NOT EXISTS public.profiles(
  id uuid DEFAULT gen_random_uuid() PRIMARY KEY,
  first_name VARCHAR(255),
  middle_name VARCHAR(255),
  last_name VARCHAR(255),
  sex VARCHAR(1),
  created_at TIMESTAMP DEFAULT NOW()
);
