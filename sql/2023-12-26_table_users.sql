-- Удаление отчества из таблицы users

ALTER TABLE IF EXISTS public.users
DROP COLUMN IF EXISTS middle_name;
