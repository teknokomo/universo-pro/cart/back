class Credential {
  #provider_name;
  #provider_user_id;
  #user_id;
  #created_at;

  static async findOneById(id) {
    const query = {
      text: `
        SELECT *
        FROM credentials
        WHERE id = $1
      `,
      values: [id],
    }

    const result = await pool.query(query); // Result
    // const result = pool.query(query); // Promise
    console.log('result', typeof result, result);

    let credential = new Credential(result.rows[0]);

    return credential; // Возвращает Promise
  }

  constructor(obj) {
    this.#provider_name = obj.provider_name;
    this.#provider_user_id = obj.provider_user_id;
    this.#user_id = obj.user_id;
    this.#created_at = obj.created_at;
  }
}

module.exports = Credential
