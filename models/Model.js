// const pool = require('../db');

class Model {
//   #db_client = null;

//   #table_name = '';
//   #table_columns = [];

//   #id = '';

//   async save() {
//     if (this.#id == '') {
//       return await this.insert();
//     }
//     else {
//       return await this.update();
//     }
//   }

//   async insert() {
//     const query = {
//       text: `
//         INSERT
//         INTO ${this.#table_name} (
//           first_name,
//           last_name
//         )
//         VALUES (
//           $1,
//           $2
//         )
//         RETURNING *
//       `,
//       values: [
//         this.#first_name,
//         this.#last_name,
//       ],
//     }

//     const qres = await pool.query(query); // Result

//     if (qres.rowCount == 1) {
//       result = qres.rows[0];
//       this.#id = result.id;
//     }
//     else {
//       result = null;
//     }

//     return result;
//   }

//   async update() {
//     // Параметризированный запрос спасает от SQL-инъекций.
//     // Не вставляйте параметр прямо в запрос!
//     const query = {
//       text: `
//         UPDATE users
//         SET
//           first_name = $1::text,
//           last_name = $2::text
//         WHERE id = $3
//         RETURNING *
//       `,
//       values: [
//         this.#first_name,
//         this.#last_name,
//         this.#id,
//       ],
//     }

//     const qres = await pool.query(query); // Result

//     return qres.rowCount == 1 ? qres.rows[0] : null;
//   }

  toObject() {
    return {};
  }

  toJSON() {
    return JSON.stringify(this.toObject());
  }
}

module.exports = Model;
