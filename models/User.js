const pool = require('../db');
const Model = require('./Model');

class User extends Model {
  static TABLE_NAME = 'users';
  static TABLE_COLUMNS = [
    'id',
    'name',
    'hashed_password',
    'salt',
    'profile_id',
    'created_at'
  ];

  #dbClient = null;

  #id = null;
  #name = '';
  #hashed_password = '';
  #salt = '';
  #profile_id = null;
  #created_at = null;

  // Клиент PG нужен для транзакций
  async getDbClient() {
    if (this.#dbClient === null) {
      this.#dbClient = await pool.connect();
    }

    return this.#dbClient;
  }

  setDbClient(dbClient) {
    this.#dbClient = dbClient;
  }

  constructor(data = {}) {
    super(data);
    
    this.#id = 'id' in data ? data.id : null;
    this.#name = 'name' in data ? data.name : '';
    this.#hashed_password = 'hashed_password' in data ? data.hashed_password : '';
    this.#salt = 'salt' in data ? data.salt : '';
    this.#profile_id = 'profile_id' in data ? data.profile_id : null;
    this.#created_at = 'created_at' in data ? data.created_at : null;
  }

  getId() {
    return this.#id;
  }

  getName() {
    return this.#name;
  }

  setName(name) {
    this.#name = name.trim();
  }

  getHashedPassword() {
    return this.#hashed_password;
  }

  getSalt() {
    return this.#salt;
  }

  getProfileId() {
    return this.#profile_id;
  }

  getCreatedAt() {
    return this.#created_at;
  }

  async insert() {
    const query = {
      text: `
        INSERT
        INTO ${User.TABLE_NAME} (
          name,
          hashed_password,
          salt,
          profile_id,
          created_at
        )
        VALUES (
          $1::text,
          $2::text,
          $3::text,
          $4::uuid,
          $5::date
        )
        RETURNING *
      `,
      values: [
        this.#name,
        this.#hashed_password,
        this.#salt,
        this.#profile_id,
        this.#created_at,
      ],
    }

    const qres = await pool.query(query); // Result

    if (qres.rowCount == 1) {
      result = qres.rows[0];
      this.#id = result.id;
    }
    else {
      result = null;
    }

    return result;
  }

  async update() {
    // Параметризированный запрос спасает от SQL-инъекций.
    // Не вставляйте параметр прямо в запрос!
    const query = {
      text: `
        UPDATE users
        SET
          name = $1::text,
          hashed_password = $2::text,
          salt = $3::text,
          profile_id = $4::uuid,
          created_at = $5::date,
        WHERE id = $6
        RETURNING *
      `,
      values: [
        this.#name,
        this.#hashed_password,
        this.#salt,
        this.#profile_id,
        this.#created_at,
        this.#id,
      ],
    }

    const qres = await pool.query(query); // Result

    return qres.rowCount == 1 ? qres.rows[0] : null;
  }

  //TODO: Сделать
  async delete() {
    return null;
  }

  toObject() {
    return {
      id: this.#id,
      // replica_id: this.#replica_id,
      name: this.#name,
      hashed_password: this.#hashed_password,
      salt: this.#salt,
      profile_id: this.#profile_id,
      created_at: this.#created_at,
    };
  }

  // toJSON() {
  //   return JSON.stringify(this.toObject());
  // }
}

module.exports = User;
