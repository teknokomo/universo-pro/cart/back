// import pool from '../db';
const pool = require('../db');
const CartUser = require('./CartUser');
const Model = require('./Model');

class Cart extends Model {
  static TABLE_NAME = 'carts';
  static TABLE_COLUMNS = [
    'id',
    'name',
  ];

  #dbClient = null;

  #id = 0;
  #replica_id = 0;
  #name = '';
  #users = null;
  #items = null;

  static async findById(id) {
    const query = {
      text: `
        SELECT *
        FROM ${Cart.TABLE_NAME}
        WHERE id = $1
      `,
      values: [id],
    }

    const res = await pool.query(query); // Result
    // const res = pool.query(query); // Promise
    // console.log('res', typeof res, res);

    let cart = new Cart(res.rows[0]);

    return cart; // Возвращает Promise
  }

  constructor(data = {}) {
    super(data);

    this.#id = 'id' in data ? data.id : 0;
    this.#name = 'replica_id' in data ? data.replica_id : 0;
    this.#name = 'name' in data ? data.name : '';
  }

  // Клиент PG нужен для транзакций
  async getDbClient() {
    if (this.#dbClient === null) {
      this.#dbClient = await pool.connect();
    }

    return this.#dbClient;
  }

  setDbClient(dbClient) {
    this.#dbClient = dbClient;
  }

  getId() {
    return this.#id;
  }

  getReplicaId() {
    return this.#replica_id;
  }

  getName() {
    return this.#name;
  }

  setName(name) {
    this.#name = name.trim();
  }

  async getCartUsersData() {
    if (this.#users === null) {
      let select_cart_users_result = undefined;
      let cart_id = this.#id;
      try {
        let dbClient = await this.getDbClient();
        select_cart_users_result = await dbClient.query({
          text: `
            SELECT *
            FROM carts_users AS cu
            WHERE cu.cart_id = $1
          `,
          values: [
            cart_id,
          ]
        });
    
        // console.log(select_cart_users_result.rows);
    
        this.#users = select_cart_users_result.rows;
      }
      catch(error) {
        console.error('select cart users error', error);
      }
    }

    return this.#users;
  }

  async findCartUser(user_id) {
    let cart_users_data = await this.getCartUsersData();
    let cartUser = null;
    for (let i = 0; i < cart_users_data.length; i++) {
      if (cart_users_data[i].user_id === user_id) {
        cartUser = new CartUser(cart_users_data[i]);
      }
    }

    return cartUser;
  }

  async getCartItemsData() {
    if (this.#items === null) {
      let select_cart_items_result = null;
      try {
        let dbClient = await this.getDbClient();
        select_cart_items_result = await dbClient.query({
          text: `
            SELECT *
            FROM carts_items AS ci
            WHERE ci.cart_id = $1
          `,
          values: [
            this.#id,
          ]
        });
    
        // console.log(select_cart_users_result.rows);
    
        this.#items = select_cart_items_result.rows;
      }
      catch(error) {
        console.error('select cart items error', error);
      }
    }

    return this.#items;
  }

  async save() {
    if (this.#id === 0) {
      return await this.insert();
    }
    else {
      return await this.update();
    }
  }

  async insert() {
    const query_insert_cart = {
      text: `
        INSERT
        INTO ${Cart.TABLE_NAME} (name, replica_id)
        VALUES ($1::text, nextval('cart_replica_id'))
        RETURNING *
      `,
      values: [
        this.#name
      ],
    }

    let dbClient = await this.getDbClient();
    const result_insert_cart = await dbClient.query(query_insert_cart); // Result

    let cart_data = null;

    if (result_insert_cart.rowCount == 1) {
      cart_data = result_insert_cart.rows[0];
      this.#id = cart_data.id;
    }

    return cart_data;
  }

  async update() {
    // Параметризированный запрос спасает от SQL-инъекций.
    // Не вставляйте параметр прямо в запрос!
    const query = {
      text: `
        UPDATE ${Cart.TABLE_NAME}
        SET
          name = $1::text,
          replica_id = nextval('cart_replica_id')
        WHERE id = $2::integer
        RETURNING *
      `,
      values: [
        this.#name,
        this.#id
      ],
    }

    let dbClient = await this.getDbClient();
    const qres = await dbClient.query(query); // Result

    return qres.rowCount == 1 ? qres.rows[0] : null;
  }

  //TODO: Сделать
  async delete() {
    return null;
  }

  toObject() {
    return {
      id: this.#id,
      replica_id: this.#replica_id,
      name: this.#name,
    };
  }

  async toExtendedObject() {
    return {
      id: this.#id,
      replica_id: this.#replica_id,
      name: this.#name,
      users: await this.getCartUsersData(),
      items: await this.getCartItemsData(),
    };
  }

  // toJSON() {
  //   return JSON.stringify(this.toObject(), ' ', 2);
  // }

  async toExtendedJSON() {
    return JSON.stringify(await this.toExtendedObject(), ' ', 2);
  }
}

module.exports = Cart;
