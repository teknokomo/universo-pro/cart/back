/**
 * Класс для работы с элементом списка.
 */

const pool = require('../db');

class CartItem {
  static TABLE_NAME = 'carts_items';
  static TABLE_COLUMNS = [
    'id',
    'cart_id',
    'name',
    'quantity_plan',
    'quantity_fact',
    'picked',
  ];

  #dbClient = null;

  #id = 0;
  #cart_id = 0;
  #name = '';
  #quantity_plan = 1;
  #quantity_fact = 0;
  #picked = false;

  static async findById(id) {
    const query = {
      text: `
        SELECT *
        FROM ${CartItem.TABLE_NAME}
        WHERE id = $1
      `,
      values: [id],
    }

    const select_result = await pool.query(query); // Result

    console.log('find', select_result);

    let cartItem = null;
    
    if (select_result.rowCount === 1) {
      cartItem = new CartItem(select_result.rows[0]);
    }

    return cartItem; // Возвращает Promise
  }

  constructor(data = {}) {
    if ('id' in data) this.setId(data.id);
    if ('cart_id' in data) this.setCartId(data.cart_id);
    this.#name = 'name' in data ? data.name : '';
    this.#quantity_plan ='quantity_plan' in data ? data.quantity_plan : 1;
    this.#quantity_fact = 'quantity_fact' in data ? data.quantity_fact : 0;
    this.#picked = 'picked' in data ? data.picked : false;
  }

  // Клиент PG нужен для транзакций
  async getDbClient() {
    if (this.#dbClient === null) {
      this.#dbClient = await pool.connect();
    }

    return this.#dbClient;
  }

  setDbClient(dbClient) {
    this.#dbClient = dbClient;
  }

  getId() {
    return this.#id;
  }

  setId(id) {
    this.#id = parseInt(id);
  }

  getCartId() {
    return this.#cart_id;
  }

  setCartId(id) {
    this.#cart_id = parseInt(id);
  }

  getName() {
    return this.#name;
  }

  setName(name) {
    this.#name = name.trim();
  }

  getQuantityPlan() {
    return this.#quantity_plan;
  }

  setQuantityPlan(quantity) {
    this.#quantity_plan = quantity;
  }

  getQuantityFact() {
    return this.#quantity_fact;
  }

  setQuantityFact(quantity) {
    this.#quantity_fact = quantity;
  }

  isPicked() {
    return this.#picked;
  }

  setPicked() {
    this.#picked = true;
  }

  setNotPicked() {
    this.#picked = false;
  }

  async save() {
    if (this.#id === 0) {
      return await this.insert();
    }
    else {
      return await this.update();
    }
  }

  // ::uuid, ::integer и ::boolean это приведение типов в PostgreSQL.
  async insert() {
    console.log('insert cart item');
    console.log(this.toObject());
    const query = {
      text: `
        INSERT INTO carts_items (cart_id, name, quantity_plan, quantity_fact, picked)
        VALUES ($1::int, $2::text, $3, $4, $5::boolean)
        RETURNING *
      `,
      values: [
        this.#cart_id,
        this.#name,
        this.#quantity_plan,
        this.#quantity_fact,
        this.#picked,
      ]
    };

    let result_insert = null;
    try {
      let dbClient = await this.getDbClient();
      result_insert = await dbClient.query(query); // Result
    }
    catch(error) {
      console.log('error', error);
    }

    let result = null;
    if (
      result_insert !== null
      && result_insert.rowCount == 1
    ) {
      result = result_insert.rows[0];
      this.#id = result.id;
    }

    return result;
  }

  async update() {
    console.log('update cart item');
    console.log(this.toObject());
    // Параметризированный запрос спасает от SQL-инъекций.
    // Не вставляйте параметр прямо в запрос!
    const query = {
      text: `
        UPDATE carts_items
        SET
          cart_id = $1::integer,
          name = $2::text,
          quantity_plan = $3,
          quantity_fact = $4,
          picked = $5::boolean
        WHERE id = $6::integer
        RETURNING *
      `,
      values: [
        this.#cart_id,
        this.#name,
        this.#quantity_plan,
        this.#quantity_fact,
        this.#picked,
        this.#id,
      ],
    };

    let result_update = null;
    try {
      let dbClient = await this.getDbClient();
      result_update = await dbClient.query(query); // Result
    }
    catch(error) {
      console.log('error', error);
    }

    let result = null;
    if (
      result_update !== null
      && result_update.rowCount == 1
    ) {
      result = result_update.rows[0];
    }

    return result;
  }

  async delete() {
    const query = {
      text: `
        DELETE FROM carts_items
        WHERE id = $1::integer
        RETURNING *
      `,
      values: [
        this.#id,
      ],
    };

    let delete_result = null;
    try {
      let dbClient = await this.getDbClient();
      delete_result = await dbClient.query(query); // Result
    }
    catch(error) {
      console.log('error', error);
    }

    let result = null;
    if (
      delete_result !== null
      && delete_result.rowCount == 1
    ) {
      result = delete_result.rows[0];
    }

    return result;
  }

  toObject() {
    return {
      id: this.#id,
      cart_id: this.#cart_id,
      name: this.#name,
      quantity_plan: this.#quantity_plan,
      quantity_fact: this.#quantity_fact,
      picked: this.#picked,
    };
  }

  toJSON() {
    return JSON.stringify(this.toObject(), ' ', 2);
  }
}

module.exports = CartItem;
