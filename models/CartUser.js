/**
 * Класс для работы с пользователем списка.
 */

const pool = require('../db');

class CartUser {
  static TABLE_NAME = 'carts_users';
  static TABLE_COLUMNS = [
    'user_id',
    'cart_id',
    'is_owner',
  ];

  #dbClient = null;

  #user_id = '';
  #cart_id = 0;
  #is_owner = false;

  constructor(data = {}) {
    this.#user_id = 'user_id' in data ? data.user_id : '';
    this.#cart_id = 'cart_id' in data ? data.cart_id : 0;
    this.#is_owner = 'is_owner' in data ? data.is_owner : false;
  }

  // Клиент PG нужен для транзакций
  async getDbClient() {
    if (this.#dbClient === null) {
      this.#dbClient = await pool.connect();
    }

    return this.#dbClient;
  }

  setDbClient(dbClient) {
    this.#dbClient = dbClient;
  }

  getUserId() {
    return this.#user_id;
  }

  getCartId() {
    return this.#cart_id;
  }

  isOwner() {
    return this.#is_owner;
  }

  setIsOwner() {
    this.#is_owner = true;
  }

  setNotOwner() {
    this.#is_owner = false;
  }

  // ::uuid, ::integer и ::boolean это приведение типов в PostgreSQL.
  async insert() {
    const query = {
      text: `
        INSERT
        INTO ${CartUser.TABLE_NAME} (
          user_id,
          cart_id,
          is_owner
        )
        VALUES (
          $1::uuid,
          $2::integer,
          $3::boolean
        )
        RETURNING *
      `,
      values: [
        this.#user_id,
        this.#cart_id,
        this.#is_owner,
      ],
    }

    let result_insert = null;
    try {
      let dbClient = await this.getDbClient();
      result_insert = await dbClient.query(query); // Result
    }
    catch(error) {
      console.log('error', error);
    }

    let result = null;
    if (
      result_insert !== null
      && result_insert.rowCount == 1
    ) {
      result = result_insert.rows[0];
    }

    return result;
  }

  async update() {
    // Параметризированный запрос спасает от SQL-инъекций.
    // Не вставляйте параметр прямо в запрос!
    const query = {
      text: `
        UPDATE ${CartUser.TABLE_NAME}
        SET
          is_owner = $1::boolean
        WHERE
          user_id = $2::uuid
          AND cart_id = $3::integer
        RETURNING *
      `,
      values: [
        this.#is_owner,
        this.#user_id,
        this.#cart_id,
      ],
    }

    let result_update = null;
    try {
      let dbClient = await this.getDbClient();
      result_update = await dbClient.query(query); // Result
    }
    catch(error) {
      console.log('error', error);
    }

    let result = null;
    if (
      result_update !== null
      && result_update.rowCount == 1
    ) {
      result = result_update.rows[0];
    }

    return result;
  }

  toObject() {
    return {
      user_id: this.#user_id,
      cart_id: this.#cart_id,
      is_owner: this.#is_owner,
    };
  }

  toJSON() {
    return JSON.stringify(this.toObject(), ' ', 2);
  }
}

module.exports = CartUser;
