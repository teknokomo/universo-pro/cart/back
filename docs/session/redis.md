# Использование Redis как хранилища для сессионных данных
Установка Redis
```
sudo docker run \
  --name cup-cache01p \
  --network cup-net1 \
  --publish 127.0.0.1:6379:6379 \
  --detach \
  redis:7.2.3-alpine3.19
```

Для боевого сервера нужно периодически сбрасывать данные на диск
```
sudo docker run \
  --name cup-cache01p \
  --network cup-net1 \
  --publish 127.0.0.1:6379:6379 \
  --detach \
  redis:7.2.3-alpine3.19 redis-server --save 60 1 --loglevel warning

```

Если нужно подключиться к Redis
```
sudo docker exec -it cup-cache01p redis-cli
```

Установка пакетов для работы с Redis

```
npm install redis connect-redis express-session
```

