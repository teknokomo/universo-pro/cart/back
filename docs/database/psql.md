# Подключиться к БД
```
psql -h 127.0.0.1 -U admin -d wms
```

# Вывести список БД
```
\l
```

# Вывести список пользователей
```
\du
```

# Создать БД
```
CREATE DATABASE wms;
```

# Подключиться к БД
```
\connect wms;
```

# Вывести список таблиц
```
\dt
```
# Файлы PostgreSQL в контейнере
данные - /var/lib/postgresql/data
исполняемый файл - /usr/local/bin/postgres
