# PG - клиент NodeJS для подключения к СУБД PostgreSQL
[Документация](https://node-postgres.com/)
# Установить пакет pg
```
npm install pg
```

# Подключение к СУБД

# Вопросы
Начал изучать backend и появился в способе обращении в бд. В чем разница между Pool и Client в node-postgres?
Что где лучше использовать?

Client - одно соединение к БД.
Pool - несколько Client

Pool позволяет делать паралельно несколько (независимых друг от друга) запросов. Если на сервис приходит хоть какая-то конкурентная нагрузка, то лучше использовать его.


Если выполнять транзакции, то нужно использовать одного и того же клиента.

Пример
```
import { Pool } from 'pg'
const pool = new Pool()
 
const client = await pool.connect()
 
try {
  await client.query('BEGIN')
  const queryText = 'INSERT INTO users(name) VALUES($1) RETURNING id'
  const res = await client.query(queryText, ['brianc'])
 
  const insertPhotoText = 'INSERT INTO photos(user_id, photo_url) VALUES ($1, $2)'
  const insertPhotoValues = [res.rows[0].id, 's3.bucket.foo']
  await client.query(insertPhotoText, insertPhotoValues)
  await client.query('COMMIT')
} catch (e) {
  await client.query('ROLLBACK')
  throw e
} finally {
  client.release()
}
```

Запрос
```
INSERT
INTO carts (name)
VALUES ($1::text)
RETURNING *
```

Структура объекта Result
```
Result {
  command: 'INSERT',
  rowCount: 1,
  oid: 0,
  rows: [ { id: 8, name: 'Test3' } ],
  fields: [
    Field {
      name: 'id',
      tableID: 16386,
      columnID: 1,
      dataTypeID: 23,
      dataTypeSize: 4,
      dataTypeModifier: -1,
      format: 'text'
    },
    Field {
      name: 'name',
      tableID: 16386,
      columnID: 2,
      dataTypeID: 1043,
      dataTypeSize: -1,
      dataTypeModifier: 259,
      format: 'text'
    }
  ],
  _parsers: [ [Function: parseInteger], [Function: noParse] ],
  _types: TypeOverrides {
    _types: {
      getTypeParser: [Function: getTypeParser],
      setTypeParser: [Function: setTypeParser],
      arrayParser: [Object],
      builtins: [Object]
    },
    text: {},
    binary: {}
  },
  RowCtor: null,
  rowAsArray: false
}
```
