# Установка БД

Установить Docker

Создать сеть для проекта
```
sudo docker network create -d bridge cup-net1
```
cup - cart.universo.pro

Создать хранилища для данных БД и для резервных копий
```
sudo docker volume create cup-postgresql-data-volume
sudo docker volume create cup-backup-volume
```

Создать контейнер
```
export POSTGRES_PASSWORD=$(openssl rand -base64 32 | cut -c1-12)
echo $POSTGRES_PASSWORD
sudo docker run \
  --name cup-db01p \
  --volume cup-postgresql-data-volume:/var/lib/postgresql/data \
  --volume cup-backup-volume:/backup \
  --env POSTGRES_DB=wms \
  --env POSTGRES_USER=admin \
  --env POSTGRES_PASSWORD=$POSTGRES_PASSWORD \
  --network cup-net1 \
  --publish 127.0.0.1:5432:5432 \
  --detach \
  postgres:15.3-alpine3.18

```
