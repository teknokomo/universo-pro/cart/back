# Проверка (validation) и очистка (sanitize) значений форм

Установить пакет express-validator
```
npm install express-validator
```

Нужна для того чтобы пользователь не мог делать инъекции кода и запросов. То есть для
безопасности сайта.

## Использование

Импортируем функции проверки и вывода результатов проверки
```
const { check, validationResult } = require('express-validator');
```

[Полный список функций проверок и очисток](https://github.com/validatorjs/validator.js)

[Примеры использования](https://heynode.com/tutorial/how-validate-and-sanitize-expressjs-form/)
