// Описание библиотеки pg
// https://node-postgres.com/

// Описание переменных окружения для libpg
// https://postgrespro.ru/docs/postgresql/9.6/libpq-envars

const { Pool } = require('pg');

const config = {
  host:     process.env.PGHOST      || '127.0.0.1',
  port:     process.env.PGPORT      || 5432,
  database: process.env.PGDATABASE  || 'wms',
  user:     process.env.PGUSER      || 'admin',
  password: process.env.PGPASSWORD  || 'secret',
};

const pool = new Pool(config);

module.exports = pool;
