// Стандарты написания кода на JS от Google
// https://google.github.io/styleguide/jsguide.html

// Стандарты для отдаваемых структур JSON
// https://swagger.io/specification/
// https://jsonapi.org/format/
// https://google.github.io/styleguide/jsoncstyleguide.xml

// Статья по классам JS
// https://dmitripavlutin.com/javascript-classes-complete-guide/

'use strict';

// Подключение модуля dotenv. Включена перезапись существующих переменных значениями из файла
require('dotenv').config({ override: true });

const express = require('express'); // фреймворк ExpressJS
const cors = require('cors'); // CORS
const session = require('express-session'); // поддержка сессий
const RedisStore = require("connect-redis").default
const {createClient}  = require("redis");

// Подключение маршрутов
const authRoutes = require('./routes/universo-auth-routes');
const cartsApiRoutes = require('./routes/carts-api-routes');

const APP_INTERFACE = process.env.APP_INTERFACE || '127.0.0.1';
const APP_PORT = process.env.APP_PORT || 3000;

const REDIS_HOST = '127.0.0.1';
const REDIS_PORT = 6379;

// Для того чтобы Express не выводил трассировку ошибок, нужно создать переменную среды NODE_ENV
// и присвоить ей значение 'production'. По умолчанию считается, что установлено значение 'development'.
// export NODE_ENV=production
// export NODE_ENV=development

const app = express();

// Настройка CORS

app.use(
  cors({
    origin: true, // зеркалировать значение заголовка Origin
    credentials: true, // разрешено передавать куки через
  }),
);

// Настройка сессии

// Warning The default server-side session storage, MemoryStore, is purposely not designed
// for a production environment. It will leak memory under most conditions, does not scale
// past a single process, and is meant for debugging and developing.

// Initialize client.
// https://npmjs.com/package/redis#usage
let redisClient = createClient({
  url: `redis://${REDIS_HOST}:${REDIS_PORT}`
});
redisClient.connect().catch(console.error);

// Initialize store.
let redisStore = new RedisStore({
  client: redisClient,
  prefix: "myapp:",
});

const oneDay = 1000 * 60 * 60 * 24;

var session_config = {
  store: redisStore,
  resave: false, // required: force lightweight session keep alive (touch)
  secret: process.env.LEARN_UNIVERSO_PRO_SESSION_SECRET || 'iVPu588YBhlJFuIB',
  // saveUninitialized: false, // recommended: only save session when data exists
  saveUninitialized: true,
  cookie: {
    path: '/', // по умолчанию = '/'
    maxAge: oneDay, // по умолчанию = null
    httpOnly: true, // по умолчанию = true
    secure: false, // по умолчанию = false
  },
  name: 'cart-sid',
};

if (app.get('env') === 'production') {
  app.set('trust proxy', 1); // доверять первому прокси (Nginx)
  session_config.cookie.secure = true; // serve secure cookies
}

app.use(session(session_config));

// Весь статический контент находится в папке public
// В этой папке нельзя делать подпапки. Всё нужно класть в корень. Иначе будут проблемы с определением типа файла.
// app.use('/css', express.static('public/css'));

app.use(express.static('public'));

app.use(express.json()); // декодирует данные в формате JSON
app.use(express.urlencoded({ extended: true })); // декодирует данные в формате URL-encoded

// Подключение маршрутов /api/carts
app.use(cartsApiRoutes);
app.use(authRoutes);

app.listen(APP_PORT, APP_INTERFACE);
console.log(`running on http://${APP_INTERFACE}:${APP_PORT}`);
